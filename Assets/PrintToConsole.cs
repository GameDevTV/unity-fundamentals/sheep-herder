using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrintToConsole : MonoBehaviour
{
    string myName = "Dog";
    int score = 1;
    
    void Start()
    {
        // Debug.Log("My name is: " + myName);
    }

    void Update()
    {
        score++;
        Debug.Log("Hello my name is " + myName + " and my score is " + score);
    }
}
